#pragma once

#include <cstdio>
#include <iostream>
#include "cryptopp565\osrng.h"
#include "cryptopp565\modes.h"
#include <string.h>
#include <cstdlib>

using namespace std;


class CryptoDevice
{

public:
	CryptoDevice();
	~CryptoDevice();
	string encryptAES(string);
	string decryptAES(string);

};
